## 用Java编写的CMS系统服务端简单说明


### 项目简介

- J2EE课程期末项目的服务端
- 用Java编写的简单的CMS（内容管理）系统

### 功能特征

- 采用`Java Web`最新框架`MVVM`，是`MVC`的升级版
- 采用`Spring Boot`框架
- 使用`MAVEN`进行管理

### 环境依赖

- `OS Windows x64`
- `Jdk 1.8 x64`
- `Spring Boot 2.0`
- `Maven 3.6`
- `数据库：SQLite`

### 部署步骤

- 本地部署

下载项目到本地，使用IDEA（`2017版本及以上`）导入项目打开，等待依赖加载完成即可启动；默认启动端口为6060
- 云部署

由于项目`还未完成`，暂时不提供`JAR`包。可以自行打包部署

### 演示地址

- [我的民族文化之旅](https://www.mzwhzy.com)  
- 或者浏览器输入：www.mzwhzy.com

### 我的个人技术博客

- [微笑涛声](https://www.cztcms.cn)  
- 或者浏览器输入：www.cztcms.cn
