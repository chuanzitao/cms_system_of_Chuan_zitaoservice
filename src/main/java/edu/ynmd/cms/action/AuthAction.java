package edu.ynmd.cms.action;

import edu.ynmd.cms.tools.JwtUtil;
import edu.ynmd.cms.tools.Tools;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Collection;
import java.util.HashMap;

//令牌环更新
@CrossOrigin
@RestController
//@PreAuthorize("hasAuthority('admin')")//只允许有admin角色的用户访问 hasAnyAuthority([auth1,auth2])
@PreAuthorize("hasAnyAuthority('admin','member')")
@RequestMapping("/auth")
public class AuthAction {
    //更新令牌环信息
    @GetMapping("refreshToken")
    @ResponseBody
    public HashMap<String,String> refreshToken( HttpServletRequest request){

        String role=null; //初始化用户角色为空
                        //getAuthorities()方法，这个方法将返回此用户的所拥有的权限。
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)    SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            role = authority.getAuthority();

        }
        // UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        String userid= (String)SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        if(Tools.isNullOrSpace(role)){     //判断role是否为空
            return new HashMap<String,String>(){{
                put("token","error");      //返回错误消息
            }};
        }
        else{ //生成新的令牌环
            String jwt=""; //初始化令牌环为空
            jwt= JwtUtil.generateToken(role,userid,60*60*1000);//令牌环有效期为一小时
            HashMap<String,String> m=new HashMap<>();
            m.put("token",jwt); //控制台打印令牌环
            return m;
        }

    }

    //获取当前登录用户的角色
    @GetMapping("getRole")
    @ResponseBody
    public HashMap<String,String> getRoleByToken(){

        String role="";
        String userid="";
                        //getAuthorities()方法，这个方法将返回此用户的所拥有的权限。
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)    SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            role = authority.getAuthority();  //获取当前登录用户的角色
        }
        if(Tools.isNullOrSpace(role)){   //如果用户角色为空，返回错误
            return new HashMap<String,String>(){{
                put("role","error");
            }};
        }
        else{
            HashMap<String,String> m=new HashMap<>();
            m.put("role",role);  //控制台打印用户角色
            return m;
        }

    }



}

