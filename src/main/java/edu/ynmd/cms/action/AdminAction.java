package edu.ynmd.cms.action;

import edu.ynmd.cms.model.*;
import edu.ynmd.cms.service.ManageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

//定义请求头为 manage ，前端管理页面
//@RequestMapping("/manage")
//@CrossOrigin
//@RestController

@CrossOrigin
@RestController
@PreAuthorize("hasAuthority('admin')") //配置角色，拥有该角色的用户方可访问
@RequestMapping("/manage")

public class AdminAction {
    @Autowired
    private ManageService manageService; //导入定义好的方法

    //新闻表保存和修改操作
    @PostMapping("saveNews")
    @ResponseBody
    public HashMap saveNews(@RequestBody News news)throws Exception{
        HashMap m=new HashMap();
        try {
            //添加发布时间
            news.setPbdate(System.currentTimeMillis());
            manageService.saveNews(news);
            m.put("msg","ok");  //保存成功返回 msg,ok
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");  //保存失败返回 msg,error
        }
        return m;
    }

    //新闻表删除操作
    @PostMapping("deleteNews")
    @ResponseBody
    public HashMap deleteNews(@RequestBody News news) throws Exception{
        HashMap m=new HashMap();
  try {
            manageService.deleteNews(news.getNewsid());
            m.put("msg","ok"); //删除成功返回 msg,ok
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error"); //删除失败返回 msg,error
        }
        return m;
    }

    //轮播区图片上传
    @PostMapping("uploadPic")
    @ResponseBody
    public HashMap uploadPic(@RequestBody MultipartFile uploadfile) throws Exception{
        HashMap m=new HashMap();
       String finename=uploadfile.getOriginalFilename();
       String suffixname=uploadfile.getOriginalFilename().substring(finename.lastIndexOf("."));
       finename=String.valueOf(System.currentTimeMillis())+suffixname;  //图片名称的组成，当前的时间戳
//        String filepath="d:/javaproject/springbootupload/"; //图片路径
        String filepath="/usr/server/img/"; //图片路径

        File tf=new File(filepath);
        if(!tf.exists()){
            tf.mkdir();
        }
        try {
            uploadfile.transferTo(new File(filepath+finename));
            m.put("msg","ok");
            m.put("filename",finename);
            return m;
        } catch (IOException e) {
            e.printStackTrace();
            m.put("msg","ioerror");
        } catch (IllegalStateException e) {
            e.printStackTrace();
            m.put("msg","illeageerror");
        }
        return m;
    }






    //轮播区添加和修改操作
    @PostMapping("saveCarousel")
    @ResponseBody
    public HashMap saveCarousel(@RequestBody Carousel carousel) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.saveCarousel(carousel);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //轮播区删除操作
    @PostMapping("deleteCarousel")
    @ResponseBody
    public HashMap deleteCarousel(@RequestBody Carousel carousel) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deleteCarousel(carousel.getCarouselid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //单页区添加和修改操作
    @PostMapping("saveSinglePage")
    @ResponseBody
    public HashMap saveSinglePage(@RequestBody Singlepage singlepage) throws Exception{
        HashMap m=new HashMap();
        try {
            singlepage.setPbdate(System.currentTimeMillis());
            manageService.saveSinglePage(singlepage);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //单页区删除操作
    @PostMapping("deleteSinglePage")
    @ResponseBody
    public HashMap deleteSinglePage(@RequestBody Singlepage singlepage) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deleteSinglePage(singlepage.getSinglepageid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //导航区添加和修改操作
    @PostMapping("saveNavigate")
    @ResponseBody
    public HashMap saveNavigate(@RequestBody Navigate navigate) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.saveNavigate(navigate);
            m.put("msg","ok");
} catch (Exception e) {
        e.printStackTrace();
        m.put("msg","error");
        }
        return m;
        }


    //导航区删除操作
    @PostMapping("deleteNavigate")
    @ResponseBody
    public HashMap deleteNavigate(@RequestBody Navigate navigate) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deletenavigate(navigate.getNaid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //民族产品区添加和修改操作
    @PostMapping("saveProduct")
    @ResponseBody
    public HashMap saveProduct(@RequestBody Product product) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.saveProduct(product);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //民族产品区删除操作
    @PostMapping("deleteProduct")
    @ResponseBody
    public HashMap deleteProduct(@RequestBody Product product) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deleteproduct(product.getProductid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //视频区添加和修改操作
    @PostMapping("saveMedia")
    @ResponseBody
    public HashMap saveMedia(@RequestBody Media media) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.saveMedia(media);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //视频区删除操作
    @PostMapping("deleteMedia")
    @ResponseBody
    public HashMap deleteMedia(@RequestBody Media media) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deleteMedia(media.getMediaid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //Logo添加和修改操作
    @PostMapping("saveLogo")
    @ResponseBody
    public HashMap saveLogo(@RequestBody Logo logo) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.saveLogo(logo);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //Logo删除操作
    @PostMapping("deleteLogo")
    @ResponseBody
    public HashMap deleteLogo(@RequestBody Logo logo) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deleteLogo(logo.getLogoid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //底部版权区域添加和修改操作
    @PostMapping("saveFooter")
    @ResponseBody
    public HashMap saveFooter(@RequestBody Footer footer) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.saveFooter(footer);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //底部版权区域删除操作
    @PostMapping("deleteFooter")
    @ResponseBody
    public HashMap deleteFooter(@RequestBody Footer footer) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deleteFooter(footer.getFooterid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //民族图片展示区添加和修改操作
    @PostMapping("savePicture")
    @ResponseBody
    public HashMap savePicture(@RequestBody Picture picture) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.savePicture(picture);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //民族图片展示区域删除操作
    @PostMapping("deletePicture")
    @ResponseBody
    public HashMap deletePicture(@RequestBody Picture picture) throws Exception{
        HashMap m=new HashMap();
        try {
            manageService.deletePicture(picture.getPictureid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }

    //JWT框架测试方法
    @GetMapping("testSecurityResource")
    @ResponseBody
    public String testSecurityResource() throws Exception{
        String userid=manageService.getCurrentUserId();
        String role=manageService.getCurrentRole();
        return "受保护的资源,当前用户的id是"+userid+"当前用户的角色是"+role;
    }


}
