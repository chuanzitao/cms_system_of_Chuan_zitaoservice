package edu.ynmd.cms.action;

import edu.ynmd.cms.model.*;
import edu.ynmd.cms.service.ManageService;
import edu.ynmd.cms.tools.BaseImgTools;
import edu.ynmd.cms.tools.JwtUtil;
import edu.ynmd.cms.vo.PageParmVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.sql.Timestamp;
//定义请求头为 public ，前端展示页面
@RequestMapping("/public")
@CrossOrigin
@RestController
public class PublicAction {

    @Autowired
    private ManageService manageService;//导入定义好的方法

    //新闻表多个查询实现方法
    @GetMapping("getNewsList")
    @ResponseBody
    public List<News> getNewsList() throws Exception{
        return manageService.getNewsList();//所有信息的查询结果返回到列表
    }

    //新闻表单个查询实现方法
    @PostMapping("getSingleNewsById")
    @ResponseBody
    public News getSingleNewsById(@RequestBody News news) throws Exception{
        return manageService.getNews(news.getNewsid()); //返回对应的ID的所有信息
    }

    //新闻表数据分页
    @PostMapping("getNewsListByPage")
    @ResponseBody
    public HashMap getNewsListByPage(@RequestBody PageParmVo pageParmVo) throws Exception{
        HashMap m=new HashMap();
        Page<News> newspage= null;
        try {
            newspage = manageService.getNewsList(pageParmVo.getPage(),pageParmVo.getPagesize());
            m.put("msg","ok");
            m.put("obj",newspage);
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }
        return m;
    }


    //单页区多个查询实现方法
    @GetMapping("getSinglePageList")
    @ResponseBody
    public List<Singlepage> getSinglePageList() throws Exception{
        return manageService.getSinglePageList();
    }

    //单页区单个查询实现方法
    @PostMapping("getSinglePageById")
    @ResponseBody
    public Singlepage getSinglePageById(@RequestBody Singlepage singlepage) throws Exception{
        return manageService.getSinglePage(singlepage.getSinglepageid());
    }

    //轮播区获得单个对象
    @PostMapping("getSingleCarousel")
    @ResponseBody
    public HashMap getSingleCarousel(@RequestBody Carousel carousel) throws Exception{
        HashMap m=new HashMap();
        Carousel temp=manageService.getCarousel(carousel.getCarouselid());
        if(temp!=null){
            m.put("msg","ok");
            m.put("obj",temp);
        }
        else {
            m.put("msg","error");
        }
        return m;
    }

    //轮播区多个查询实现方法--弃用
    @GetMapping("getCarouselList")
    @ResponseBody
    public List<Carousel>  getCarouselList() throws Exception{
        return manageService.getCarouselList();
    }

    //轮播区多个查询实现方法--重写
    @GetMapping("getCarouselListMap")
    @ResponseBody
    public HashMap getgetCarouselListMap() throws Exception{
        HashMap m=new HashMap();
        List<Carousel> cl=manageService.getCarouselList();
        m.put("msg","ok");
        m.put("list",cl);
        return m;
    }

    //多媒体区多个查询方法
    @PostMapping("getMediaListByType")
    @ResponseBody
    public List<Media> getMediaListByType(@RequestBody Media media) throws Exception{
        return manageService.getMediaListByType(media.getType());
    }

    //多媒体区单个查询方法
    @PostMapping("getMediaListById")
    @ResponseBody
    public Media getMediaListById(@RequestBody Media media) throws Exception{
        return manageService.getMedia(media.getMediaid());
    }

    //民族产品区多个查询方法
    @GetMapping("getProductList")
    @ResponseBody
    public List<Product> getProductList() throws Exception{
        return manageService.getProductlist();
    }

    //民族产品区单个查询方法
    @PostMapping("getProductById")
    @ResponseBody
    public Product getProductById(@RequestBody Product product) throws Exception{
        return manageService.getProduct(product.getProductid());
    }

    //导航区多个查询方法
    @GetMapping("getNavigateList")
    @ResponseBody
    public List<Navigate> getNavigateList() throws Exception{
        return manageService.getNavigatelist();
    }

    //导航区单个查询方法
    @PostMapping("getNavigateById")
    @ResponseBody
    public Navigate getNavigateById(@RequestBody Navigate navigate) throws Exception{
        return manageService.getNavigate(navigate.getNaid());
    }

    //Logo区多个查询方法
    @GetMapping("getLogoList")
    @ResponseBody
    public List<Logo> getLogoList() throws Exception{
        return manageService.getLogolist();
    }

    //Logo区单个查询方法
    @PostMapping("getLogoById")
    @ResponseBody
    public Logo getLogoById(@RequestBody Logo logo) throws Exception{
        return manageService.getLogo(logo.getLogoid());
    }

    //底部版权区域多个查询方法
    @GetMapping("getFooterList")
    @ResponseBody
    public List<Footer> getFooterList() throws Exception{
        return manageService.getFooterlist();
    }

    //底部版权区域单个查询方法
    @PostMapping("getFooterById")
    @ResponseBody
    public Footer getFooterById(@RequestBody Footer footer) throws Exception{
        return manageService.getFooter(footer.getFooterid());
    }


    //民族图片展示区多个查询方法
    @GetMapping("getPictureList")
    @ResponseBody
    public List<Picture> getPictureList() throws Exception{
        return manageService.getPicturelist();
    }

    //民族图片展示区单个查询方法
    @PostMapping("getPictureById")
    @ResponseBody
    public Picture getPictureById(@RequestBody Picture picture) throws Exception{
        return manageService.getPicture(picture.getPictureid());
    }

    //用户登录方法
    @PostMapping("/login")
    @ResponseBody
    public HashMap<String,String> login(
            @RequestBody Account account) throws IOException {
        Users u=manageService.getUserByUserNameAndPass(account.username,account.password); //获取用户表的用户名和密码
        if(u!=null){
            String jwt= JwtUtil.generateToken(u.getRoleid(),u.getUsersid());  //获取用户角色和用户ID

            return new HashMap<String,String>(){{
                put("msg","ok");
                put("token",jwt);
                put("role",u.getRoleid());
                // put("role","admin");
            }};
        }
        else {
            return new HashMap<String,String>(){{
                put("msg","error");
                put("token","error");
            }};
        }
    }

    public static class Account{
        public String username;
        public String password;
    }

    //用户注册
    @PostMapping("saveUser")
    @ResponseBody
    public HashMap saveUser(@RequestBody Users users) throws Exception{
        HashMap m=new HashMap();

        try {
            users.setRoleid("member");

            manageService.saveUser(users);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();

            m.put("msg","error");
        }


        return m;
    }

    @PostMapping("saveMyFrinds")
    @ResponseBody
    public HashMap saveMyFrinds(@RequestBody Myfriend myfriend) throws Exception{
        HashMap m=new HashMap();
        myfriend.setPbtime(new Timestamp(System.currentTimeMillis()).getTime());


        try {
            manageService.saveMyfriend(myfriend);
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }


        return m;
    }

    @PostMapping("deteMyFriends")
    @ResponseBody
    public HashMap deleteMyFriends(@RequestBody Myfriend myfriend) throws Exception{

        HashMap m=new HashMap();

        try {
            manageService.deleteMyFriends(myfriend.getMyfriendid());
            m.put("msg","ok");
        } catch (Exception e) {
            e.printStackTrace();
            m.put("msg","error");
        }

        return m;
    }


    @PostMapping("getSingleMyFriends")
    @ResponseBody
    public HashMap getSingleMyFriends(@RequestBody Myfriend myfriend) throws Exception{

        HashMap m=new HashMap();

        Myfriend temp=  manageService.getMyfrindById(myfriend.getMyfriendid());




        if(temp!=null){
            m.put("msg","ok");
            m.put("obj",temp);
            String[] picbase=temp.getPic().split(",");
            BaseImgTools.saveImgByStr("D:\\upload\\"+temp.getMyfriendid()+".jpg",picbase[1]);


        }
        else {
            m.put("msg","empty");
        }


        return m;
    }


    @GetMapping("getLatestMyFriends")
    @ResponseBody
    public HashMap getLatestMyFriends() throws Exception{
        HashMap m=new HashMap();
        List<Myfriend> fl=manageService.getLatestMyFrinds();

        if(fl.size()>0){
            m.put("msg","ok");
            m.put("list",fl);
        }
        else{
            m.put("msg","empty");
        }
        return m;

    }




}