package edu.ynmd.cms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableAutoConfiguration
@SpringBootApplication
//项目入口类
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class,args);
    }
}