package edu.ynmd.cms.service;

import edu.ynmd.cms.model.*;
import org.springframework.data.domain.Page;
import sun.rmi.runtime.Log;

import java.util.List;

public interface ManageService {

    //定义 新闻表 增删查改的方法
    News saveNews(News news);
    boolean deleteNews(String id);
    News getNews(String id);    //查询单个
    List<News> getNewsList();   //查询多个
    Page<News> getNewsList(int start, int pagesize);

    //定义 单页表 增删查改的方法
    Singlepage saveSinglePage(Singlepage singlepage);
    boolean deleteSinglePage(String id);
    Singlepage getSinglePage(String id);   //查询单个
    List<Singlepage> getSinglePageList();   //查询多个

    //定义 轮播区 增删查改的方法
    boolean deleteCarousel(String id);
    Carousel saveCarousel(Carousel carousel);
    Carousel getCarousel(String id);    //查询单个
    List<Carousel> getCarouselList();   //查询多个

    //定义 多媒体区 增删查改的方法
    Media saveMedia(Media media);
    boolean deleteMedia(String id);
    Media getMedia(String id); //查询单个
    List<Media> getMediaListByType(String type); //根据多媒体类型进行多个查询

    //定义 民族产品区 增删查改的方法
    Product saveProduct(Product product);
    boolean deleteproduct(String id);
    Product getProduct(String id); //查询单个
    List<Product> getProductlist(); //查询多个

    //定义 导航区 增删查改的方法
    Navigate saveNavigate(Navigate navigate);
    boolean deletenavigate(String id);
    Navigate getNavigate(String id); //查询单个
    List<Navigate> getNavigatelist(); //查询多个


    //定义 Logo区 增删查改的方法
    Logo saveLogo(Logo logo);
    boolean deleteLogo(String id);
    Logo getLogo(String id);
    List<Logo> getLogolist();

    //定义底部版权区 增删查改的方法
    Footer saveFooter(Footer footer);
    boolean deleteFooter(String id);
    Footer getFooter(String id);
    List<Footer> getFooterlist();

    // 民族图片展示区增删查改的方法
    Picture savePicture(Picture picture);
    boolean deletePicture(String id);
    Picture getPicture(String id);
    List<Picture> getPicturelist();

    String getCurrentUserId();
    String getCurrentRole();

    //用户表
    Users saveUser(Users users);
    boolean deleteUser(String id);
    Users getUser(String id);
    Users getUserByUserNameAndPass(String username,String pass);

    //朋友圈
    Myfriend saveMyfriend(Myfriend myfriend);
    boolean deleteMyFriends(String id);
    Myfriend getMyfrindById(String id);
    List<Myfriend> getLatestMyFrinds();
}