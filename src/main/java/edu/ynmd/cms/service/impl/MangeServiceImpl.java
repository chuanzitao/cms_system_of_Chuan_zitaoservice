package edu.ynmd.cms.service.impl;

import edu.ynmd.cms.dao.*;
import edu.ynmd.cms.model.*;
import edu.ynmd.cms.service.ManageService;
import edu.ynmd.cms.tools.Tools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class MangeServiceImpl implements ManageService{

    //将各个Dao进行对象化 @Autowired的作用是自动注入依赖的Bean。
    @Autowired
    private NewsDao newsDao;
    @Autowired
    private SpDao spDao;
    @Autowired
    private CarouselDao carouselDao;
    @Autowired
    private MediaDao mediaDao;
    @Autowired
    private ProductDao productDao;
    @Autowired
    private NavigateDao navigateDao;
    @Autowired
    private LogoDao logoDao;
    @Autowired
    private FooterDao footerDao;
    @Autowired
    private PictureDao pictureDao;
    @Autowired
    private UsersDao usersDao;
    @Autowired
    private MyfriendDao myfriendDao;
//*****************************新闻表增删查改开始*********************************

//新闻列表添加和修改操作
    @Override
    public News saveNews(News news) {
        try {
            return   newsDao.save(news);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //新闻列表删除操作
    @Override
    public boolean deleteNews(String id) {
        try {
            newsDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

//新闻列表查询单个操作
@Override
public News getNews(String id) {
    try {
        Optional<News> temp= newsDao.findById(id);
        return temp.get();
    } catch (Exception e) {
        e.printStackTrace();
        return null;
    }
}

// 新闻列表查询多个操作
@Override
public List<News> getNewsList() {
        return newsDao.getAll();  //返回列表
}

    // 新闻列表分页
    @Override
    public Page<News> getNewsList(int start, int pagesize) {
        return newsDao.getNewsForPage(PageRequest.of(start,pagesize));
    }

//*****************************单页表增删查改开始*********************************

    //单页表添加修改操作
    @Override
    public Singlepage saveSinglePage(Singlepage singlepage) {
        try {
            return spDao.save(singlepage);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //单页表删除操作
    @Override
    public boolean deleteSinglePage(String id) {
        try {
            spDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //单页表 单个查询操作
    @Override
    public Singlepage getSinglePage(String id) {
        try {
            Optional<Singlepage> singlepage=spDao.findById(id);
            return singlepage.get();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //单页表多个查询操作
    @Override
    public List<Singlepage> getSinglePageList() {
        return spDao.findAll();
    }


//*****************************轮播区增删查改开始*********************************

    //轮播区删除操作
    @Override
    public boolean deleteCarousel(String id) {
        try {
            carouselDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //轮播区添加和修改操作
    @Override
    public Carousel saveCarousel(Carousel carousel) {
        try {
            return carouselDao.save(carousel);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //轮播区单个查询操作
    @Override
    public Carousel getCarousel(String id) {
        try {
            Optional<Carousel> temp=carouselDao.findById(id);
            return temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //轮播区多个查询操作
    @Override
    public List<Carousel> getCarouselList() {
        return carouselDao.findAll();
    }


//*****************************多媒体区增删查改开始*********************************

    //多媒体区添加修改操作
    @Override
    public Media saveMedia(Media media) {
        try {
            return mediaDao.save(media);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    //多媒体区删除操作
    @Override
    public boolean deleteMedia(String id) {
        try {
            mediaDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //多媒体区单个查询操作
    @Override
    public Media getMedia(String id) {
        try {
            Optional<Media> temp=mediaDao.findById(id);
            return  temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //多媒体区根据多媒体属性进行同一类型多个查询
    @Override
    public List<Media> getMediaListByType(String type) {
        return mediaDao.getMediaByType(type);
    }


//*****************************民族产品区增删查改开始*********************************

   //民族产品区添加修改操作
   @Override
   public Product saveProduct(Product product) {
       try {
           return productDao.save(product);
       } catch (Exception e) {
           e.printStackTrace();
           return null;
       }
   }

    //民族产品区删除操作
    @Override
    public boolean deleteproduct(String id) {
        try {
            productDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //民族产品区单个查询操作
    @Override
    public Product getProduct(String id) {
        try {
            Optional<Product> temp=productDao.findById(id);
            return temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //民族产品区多个查询操作
    @Override
    public List<Product> getProductlist() {
        return productDao.findAll();
    }



//*****************************导航区增删查改开始*********************************

    //导航区添加修改操作
    @Override
    public Navigate saveNavigate(Navigate navigate) {
        try {
            return navigateDao.save(navigate);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //导航区删除操作
    @Override
    public boolean deletenavigate(String id) {
        try {
            navigateDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }



    //导航区单个查询操作
    @Override
    public Navigate getNavigate(String id) {
        try {
            Optional<Navigate> temp=navigateDao.findById(id);
            return temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //导航区多个查询操作
    @Override
    public List<Navigate> getNavigatelist() {
        return navigateDao.findAll();
    }

//*****************************Logo增删查改开始*********************************

    //Logo添加和修改
    @Override
    public Logo saveLogo(Logo logo) {
        try {
            return logoDao.save(logo);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //Logo删除
    @Override
    public boolean deleteLogo(String id) {
        try {
            logoDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //Logo单个查询
    @Override
    public Logo getLogo(String id) {
        try {
            Optional<Logo> temp=logoDao.findById(id);
            return temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //Logo多个查询
    @Override
    public List<Logo> getLogolist() {
        return logoDao.findAll();
    }


//*****************************底部版权区增删查改开始*********************************

    //底部版权区添加和修改
    @Override
    public Footer saveFooter(Footer footer) {
        try {
            return footerDao.save(footer);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //底部版权区删除
    @Override
    public boolean deleteFooter(String id) {
        try {
            footerDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //底部版权区单个查询
    @Override
    public Footer getFooter(String id) {
        try {
            Optional<Footer> temp=footerDao.findById(id);
            return temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //底部版权区多个查询
    @Override
    public List<Footer> getFooterlist() {
        return footerDao.findAll();
    }

//*****************************民族图片展示区增删查改开始*********************************

    //民族图片展示区添加和修改
    @Override
    public Picture savePicture(Picture picture) {
        try {
            return pictureDao.save(picture);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    //民族图片展示区删除
    @Override
    public boolean deletePicture(String id) {
        try {
            pictureDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    //民族图片展示区单个查询
    @Override
    public Picture getPicture(String id) {
        try {
            Optional<Picture> temp=pictureDao.findById(id);
            return temp.get();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //民族图片展示区多个查询
    @Override
    public List<Picture> getPicturelist() {
        return pictureDao.findAll();
    }

//*****************************用户表增删查改开始*********************************
    @Override
    public Users saveUser(Users users) {
        try {
            return usersDao.save(users);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean deleteUser(String id) {
        try {
            usersDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return false;
    }

    @Override
    public Users getUser(String id) {
        Optional<Users> temp=usersDao.findById(id);
        return temp.isPresent()?temp.get():null;
    }

    @Override
    public Users getUserByUserNameAndPass(String username, String pass) {
        List<Users> ul=usersDao.getUsersByUsernameAndPass(username,pass);
        if(ul.size()>0){
            return ul.get(0);
        }
        return null;
    }

    @Override
    public Myfriend saveMyfriend(Myfriend myfriend) {
        return myfriendDao.save(myfriend);

    }

    @Override
    public boolean deleteMyFriends(String id) {
        try {
            myfriendDao.deleteById(id);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Myfriend getMyfrindById(String id) {
        Optional<Myfriend> temp=myfriendDao.findById(id);

        return temp.isPresent()?temp.get():null;
    }

    @Override
    public List<Myfriend> getLatestMyFrinds() {
        return myfriendDao.getlatestMyfrindlist();
    }

    //获取当前登录用的的Id
    @Override
    public String getCurrentUserId() {
        String userid= (String) SecurityContextHolder.getContext().getAuthentication() .getPrincipal();
        if(Tools.isNullOrSpace(userid)){
            return null;
        }
        else {
            return userid;
        }
    }
    //获取当前登录用户的角色
    @Override
    public String getCurrentRole() {
        String role=null;
        Collection<SimpleGrantedAuthority> authorities = (Collection<SimpleGrantedAuthority>)    SecurityContextHolder.getContext().getAuthentication().getAuthorities();
        for (GrantedAuthority authority : authorities) {
            role = authority.getAuthority();

        }

        if(Tools.isNullOrSpace(role)){
            return null;
        }
        else{
            return role;
        }
    }




}