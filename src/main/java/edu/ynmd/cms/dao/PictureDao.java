package edu.ynmd.cms.dao;

import edu.ynmd.cms.model.Picture;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PictureDao extends JpaRepository<Picture,String> {

}
