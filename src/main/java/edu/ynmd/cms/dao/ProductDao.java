package edu.ynmd.cms.dao;

import edu.ynmd.cms.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProductDao extends JpaRepository<Product,String>{
}
