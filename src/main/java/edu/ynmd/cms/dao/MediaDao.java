package edu.ynmd.cms.dao;

import edu.ynmd.cms.model.Media;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface MediaDao extends JpaRepository<Media,String> {

    //使用type字段判断是视频还是音频文件，便于查询
    @Query("select m from Media m where m.type=:mtype")
    List<Media> getMediaByType(@Param("mtype") String type);

}