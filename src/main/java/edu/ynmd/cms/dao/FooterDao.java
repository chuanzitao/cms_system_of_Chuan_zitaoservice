package edu.ynmd.cms.dao;

import edu.ynmd.cms.model.Footer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FooterDao extends JpaRepository<Footer,String> {
}
