package edu.ynmd.cms.dao;

import edu.ynmd.cms.model.Navigate;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NavigateDao extends JpaRepository<Navigate,String> {
}
