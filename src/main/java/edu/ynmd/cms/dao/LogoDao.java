package edu.ynmd.cms.dao;

import edu.ynmd.cms.model.Logo;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogoDao extends JpaRepository<Logo,String> {
}
