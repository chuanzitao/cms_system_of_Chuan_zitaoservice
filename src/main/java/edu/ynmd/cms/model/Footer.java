package edu.ynmd.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Footer {
    private String footerid;
    private String title1;
    private String title2;
    private String title3;

    @Id
    //指定生成器名称
    @GeneratedValue(generator = "uuid2" )
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "footerid")
    public String getFooterid() {
        return footerid;
    }

    public void setFooterid(String footerid) {
        this.footerid = footerid;
    }

    @Basic
    @Column(name = "title1")
    public String getTitle1() {
        return title1;
    }

    public void setTitle1(String title1) {
        this.title1 = title1;
    }

    @Basic
    @Column(name = "title2")
    public String getTitle2() {
        return title2;
    }

    public void setTitle2(String title2) {
        this.title2 = title2;
    }

    @Basic
    @Column(name = "title3")
    public String getTitle3() {
        return title3;
    }

    public void setTitle3(String title3) {
        this.title3 = title3;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Footer footer = (Footer) o;

        if (footerid != null ? !footerid.equals(footer.footerid) : footer.footerid != null) return false;
        if (title1 != null ? !title1.equals(footer.title1) : footer.title1 != null) return false;
        if (title2 != null ? !title2.equals(footer.title2) : footer.title2 != null) return false;
        if (title3 != null ? !title3.equals(footer.title3) : footer.title3 != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = footerid != null ? footerid.hashCode() : 0;
        result = 31 * result + (title1 != null ? title1.hashCode() : 0);
        result = 31 * result + (title2 != null ? title2.hashCode() : 0);
        result = 31 * result + (title3 != null ? title3.hashCode() : 0);
        return result;
    }
}
