package edu.ynmd.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Navigate {
    private String naid;
    private String title;
    private String content;

    @Id
    //指定生成器名称
    @GeneratedValue(generator = "uuid2" )
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )

    @Column(name = "naid")
    public String getNaid() {
        return naid;
    }

    public void setNaid(String naid) {
        this.naid = naid;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Navigate navigate = (Navigate) o;

        if (naid != null ? !naid.equals(navigate.naid) : navigate.naid != null) return false;
        if (title != null ? !title.equals(navigate.title) : navigate.title != null) return false;
        if (content != null ? !content.equals(navigate.content) : navigate.content != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = naid != null ? naid.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}
