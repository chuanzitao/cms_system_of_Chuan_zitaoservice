package edu.ynmd.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Picture {
    private String pictureid;
    private String picurl;

    @Id
    //指定生成器名称
    @GeneratedValue(generator = "uuid2" )
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "pictureid")
    public String getPictureid() {
        return pictureid;
    }

    public void setPictureid(String pictureid) {
        this.pictureid = pictureid;
    }

    @Basic
    @Column(name = "picurl")
    public String getPicurl() {
        return picurl;
    }

    public void setPicurl(String picurl) {
        this.picurl = picurl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Picture picture = (Picture) o;

        if (pictureid != null ? !pictureid.equals(picture.pictureid) : picture.pictureid != null) return false;
        if (picurl != null ? !picurl.equals(picture.picurl) : picture.picurl != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = pictureid != null ? pictureid.hashCode() : 0;
        result = 31 * result + (picurl != null ? picurl.hashCode() : 0);
        return result;
    }
}
