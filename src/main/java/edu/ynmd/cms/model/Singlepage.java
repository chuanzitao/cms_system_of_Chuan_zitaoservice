package edu.ynmd.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Singlepage {
    private String singlepageid;
    private String title;
    private String author;
    private Long pbdate;
    private String content;
    private String cover;
    private String describe;

    @Id
    //指定生成器名称
    @GeneratedValue(generator = "uuid2" )
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "singlepageid")
    public String getSinglepageid() {
        return singlepageid;
    }

    public void setSinglepageid(String singlepageid) {
        this.singlepageid = singlepageid;
    }

    @Basic
    @Column(name = "describe")
    public String getDescribe() {
        return describe;
    }

    public void setDescribe(String describe) {
        this.describe = describe;
    }


    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "author")
    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    @Basic
    @Column(name = "pbdate")
    public Long getPbdate() {
        return pbdate;
    }

    public void setPbdate(Long pbdate) {
        this.pbdate = pbdate;
    }

    @Basic
    @Column(name = "content")
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Basic
    @Column(name = "cover")
    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Singlepage that = (Singlepage) o;

        if (singlepageid != null ? !singlepageid.equals(that.singlepageid) : that.singlepageid != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (author != null ? !author.equals(that.author) : that.author != null) return false;
        if (pbdate != null ? !pbdate.equals(that.pbdate) : that.pbdate != null) return false;
        if (content != null ? !content.equals(that.content) : that.content != null) return false;
        if (cover != null ? !cover.equals(that.cover) : that.cover != null) return false;
        if (describe != null ? !describe.equals(that.describe) : that.describe != null) return false;


        return true;
    }

    @Override
    public int hashCode() {
        int result = singlepageid != null ? singlepageid.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (author != null ? author.hashCode() : 0);
        result = 31 * result + (pbdate != null ? pbdate.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        result = 31 * result + (cover != null ? cover.hashCode() : 0);
        result = 31 * result + (describe != null ? describe.hashCode() : 0);

        return result;
    }
}
