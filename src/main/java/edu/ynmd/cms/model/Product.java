package edu.ynmd.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Product {
    private String productid;
    private String title;
    private String url;
    private String comtent;

    @Id
    //指定生成器名称
    @GeneratedValue(generator = "uuid2" )
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )

    @Column(name = "productid")
    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    @Basic
    @Column(name = "comtent")
    public String getComtent() {
        return comtent;
    }

    public void setComtent(String comtent) {
        this.comtent = comtent;
    }

    @Basic
    @Column(name = "title")
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Basic
    @Column(name = "url")
    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        if (productid != null ? !productid.equals(product.productid) : product.productid != null) return false;
        if (title != null ? !title.equals(product.title) : product.title != null) return false;
        if (url != null ? !url.equals(product.url) : product.url != null) return false;
        if (comtent != null ? !comtent.equals(product.comtent) : product.comtent != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = productid != null ? productid.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        result = 31 * result + (comtent != null ? comtent.hashCode() : 0);
        return result;
    }
}
