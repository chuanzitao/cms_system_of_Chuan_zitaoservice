package edu.ynmd.cms.model;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
public class Logo {
    private String logoid;
    private String logourl;
    private String sloganurl;
    private String url;

    @Id
    //指定生成器名称
    @GeneratedValue(generator = "uuid2" )
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator" )
    @Column(name = "logoid")
    public String getLogoid() {
        return logoid;
    }

    public void setLogoid(String logoid) {
        this.logoid = logoid;
    }

    @Basic
    @Column(name = "url")
    public String geturl() {
        return url;
    }

    public void seturl(String url) {
        this.url = url;
    }

    @Basic
    @Column(name = "logourl")
    public String getLogourl() {
        return logourl;
    }

    public void setLogourl(String logourl) {
        this.logourl = logourl;
    }

    @Basic
    @Column(name = "sloganurl")
    public String getSloganurl() {
        return sloganurl;
    }

    public void setSloganurl(String sloganurl) {
        this.sloganurl = sloganurl;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Logo logo = (Logo) o;

        if (logoid != null ? !logoid.equals(logo.logoid) : logo.logoid != null) return false;
        if (logourl != null ? !logourl.equals(logo.logourl) : logo.logourl != null) return false;
        if (sloganurl != null ? !sloganurl.equals(logo.sloganurl) : logo.sloganurl != null) return false;
        if (url != null ? !url.equals(logo.url) : logo.url != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = logoid != null ? logoid.hashCode() : 0;
        result = 31 * result + (logourl != null ? logourl.hashCode() : 0);
        result = 31 * result + (sloganurl != null ? sloganurl.hashCode() : 0);
        result = 31 * result + (url != null ? url.hashCode() : 0);
        return result;
    }
}
